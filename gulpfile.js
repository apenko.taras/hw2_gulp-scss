const gulp = require("gulp"),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync').create(),
    imagemin = require('gulp-imagemin'),
    autoprefixer = require("gulp-autoprefixer"),
    sass = require('gulp-sass'),
    uncss = require('gulp-uncss'),
    minifyCSS = require('gulp-clean-css')

sass.compiler = require('node-sass');

/*** PATHS ***/
const paths = {
    src: {
        css: "./src/scss/*.scss",
        js: "./src/js/*.js",
        img: "./src/img/*"
    },
    dist: {
        css: "./dist/css/",
        js: "./dist/js/",
        img: "./dist/img/",
        self: "./dist/"
    }
};

/*** FUNCTIONS ***/

const buildJS = () => (
    gulp.src(paths.src.js)
        .pipe(concat('script.js'))
        .pipe(gulp.dest(paths.dist.js))
        .pipe(browserSync.stream())
);

const buildCSS = () => (
    gulp.src(paths.src.css)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(uncss({
            html: ['index.html', 'posts/**/*.html', 'http://example.com']
        }))
        .pipe(minifyCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(paths.dist.css))
        .pipe(browserSync.stream())
);

const buildImgMin = () => (
    gulp.src(paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist.img))
        .pipe(browserSync.stream())
);

const cleanBuild = () => (
    gulp.src(paths.dist.self, {allowEmpty: true})
        .pipe(clean())
);

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch(paths.src.css, buildCSS).on("change", browserSync.reload);
    gulp.watch(paths.src.js, buildJS).on("change", browserSync.reload);
    gulp.watch(paths.src.img, buildImgMin).on("change", browserSync.reload);
};




/*** TASKS ***/
gulp.task("clean", cleanBuild);
gulp.task("buildCSS", buildCSS);
gulp.task("buildJS", buildJS);

const build = gulp.series(
    cleanBuild,
    buildCSS,
    buildJS,
    buildImgMin);

gulp.task("build", build)

gulp.task("default", gulp.series(
    cleanBuild,
    buildCSS,
    buildJS,
    buildImgMin,
    watcher
));
